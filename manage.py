from flaskext.actions import Manager
from panels import app


manager = Manager(app)


@manager.register('build_db')
def build_db(app):
    def action():
        """Build the database

        """
        from panels import db

        print "Building database"
        db.create_all()
        print "Database built"

    return action


if __name__ == '__main__':
    manager.run()
