import httpretty
from panels import Issue

from test.panels import PanelsTestCase
from panels.comicvine.model import ComicVine


FIXTURES = {
    'VOLUME_SEARCH': 'volume-search.json',
    'ISSUE_100001': 'issue-100001.json',
    'VOLUME_1001': 'volume-1001.json',
    'PUBLISHER_1001': 'publisher-1001.json',
}


class SearchVolumesTest(PanelsTestCase):
    def setUp(self):
        super(SearchVolumesTest, self).setUp()

        self.comic_vine = ComicVine()

    @httpretty.activate
    def test_search_volumes_success(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/search',
            body=self.load_fixture(FIXTURES['VOLUME_SEARCH'])
        )
        volumes = self.comic_vine.search_volumes('Volume')

        expected_volumes = [
            {
                'id': 1001,
                'resource_type': 'volume',
                'api_detail_url': (
                    'http://www.comicvine.com/api/volume/4050-1001'
                ),
                'name': 'Volume 1001',
                'start_year': 2011,
                'publisher': {
                    'id': 1001,
                    'name': 'Publisher 1001',
                },
                "count_of_issues": 100
            },
            {
                'id': 1002,
                'resource_type': 'volume',
                'api_detail_url': (
                    'http://www.comicvine.com/api/volume/4050-1002'
                ),
                'name': 'Volume 1002',
                'start_year': 2012,
                'publisher': {
                    'id': 1001,
                    'name': 'Publisher 1001',
                },
                "count_of_issues": 200
            },
            {
                'id': 1003,
                'resource_type': 'volume',
                'api_detail_url': (
                    'http://www.comicvine.com/api/volume/4050-1003'
                ),
                'name': 'Volume 1003',
                'start_year': 2013,
                'publisher': {
                    'id': 1001,
                    'name': 'Publisher 1001',
                },
                "count_of_issues": 300
            },
        ]

        self.assertEqual(volumes, expected_volumes)

    @httpretty.activate
    def test_search_volumes_500(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/search',
            status=500
        )
        volumes = self.comic_vine.search_volumes('Volume')

        self.assertIsNone(volumes)

    @httpretty.activate
    def test_get_issue_data_success(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/issue/4000-100001',
            body=self.load_fixture(FIXTURES['ISSUE_100001'])
        )
        issue_data = self.comic_vine.get_issue(100001)

        expected_issue_data = {
            'id': 100001,
            'api_detail_url': 'http://www.comicvine.com/api/issue/4000-100001',
            'name': 'Issue 100001',
            'cover_date': '2014-01-01',
            'issue_number': '1',
            'volume': {
                'id': 1001
            }
        }

        self.assertDictEqual(issue_data, expected_issue_data)

    @httpretty.activate
    def test_get_issue_data_500(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/issue/4000-100001',
            status=500
        )
        issue_data = self.comic_vine.get_issue(100001)

        self.assertIsNone(issue_data)

    @httpretty.activate
    def test_get_volume_data_success(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/volume/4050-1001',
            body=self.load_fixture(FIXTURES['VOLUME_1001'])
        )
        volume_data = self.comic_vine.get_volume(1001)

        self.assertEqual(volume_data['id'], 1001)
        self.assertEqual(volume_data['api_detail_url'],
                         'http://www.comicvine.com/api/volume/4050-1001')
        self.assertEqual(volume_data['name'], 'Volume 1001')
        self.assertEqual(volume_data['start_year'], 2011)
        self.assertEqual(volume_data['publisher']['id'], 1001)
        self.assertEqual(len(volume_data['issues']), 3)

        self.assertEqual(volume_data['issues'][0]['id'], 1001)
        self.assertEqual(volume_data['issues'][1]['id'], 1002)
        self.assertEqual(volume_data['issues'][2]['id'], 1003)


    @httpretty.activate
    def test_get_volume_data_500(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/volume/4050-1001',
            status=500
        )
        volume_data = self.comic_vine.get_volume(1001)

        self.assertIsNone(volume_data)

    @httpretty.activate
    def test_get_publisher_data_success(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/publisher/4010-1001',
            body=self.load_fixture(FIXTURES['PUBLISHER_1001'])
        )
        publisher_data = self.comic_vine.get_publisher(1001)

        expected_publisher_data = {
            'id': 1001,
            'api_detail_url': (
                'http://www.comicvine.com/api/publisher/4010-1001'
            ),
            'name': 'Publisher 1001'
        }

        self.assertDictEqual(publisher_data, expected_publisher_data)

    @httpretty.activate
    def test_get_publisher_data_500(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/publisher/4010-1001',
            status=500
        )
        publisher_data = self.comic_vine.get_publisher(1001)

        self.assertIsNone(publisher_data)
