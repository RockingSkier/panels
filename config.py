import os


_basedir = os.path.abspath(os.path.dirname(__file__))

DEBUG = True

SECRET_KEY = 'PeopleLikeEatingDoughnutsBecauseTheyAreTasty'

SQLALCHEMY_DATABASE_URI = 'sqlite:///{db_path}'.format(
    db_path=os.path.join(_basedir, 'panels.db')
)

COMICVINE_APIKEY = '1ff326826d5d1b479d356ef8c7d4027f2c206d86'
