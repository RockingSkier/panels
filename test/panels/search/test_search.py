import mock

from panels.search.search import Search
from test.panels import PanelsTestCase


class SearchTest(PanelsTestCase):
    def setUp(self):
        super(SearchTest, self).setUp()

        self.search = Search()

    @mock.patch('panels.search.providers.nzbindexrss.NZBIndexRSS.search')
    def test_search(self, mock_nzb_index_rss_search):
        mock_nzb_index_rss_search.return_value = [
            {
                'title': 'Search',
                'url': 'url',
            }
        ]

        search_terms = ['Search for me']
        search_filter = 'S[earch]{5}'

        results = self.search.search(search_terms, search_filter)

        self.assertTrue(mock_nzb_index_rss_search.call_count, 2)
        mock_nzb_index_rss_search.assert_called_with(
            search_terms,
            search_filter
        )

        self.assertTrue(len(results), 1)
