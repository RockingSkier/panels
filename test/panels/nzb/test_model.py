from test.panels import PanelsTestCase
from panels.nzb.model import NZB


FIXTURES = {
}


class NZBModelTest(PanelsTestCase):
    def test_create(self):
        nzb = NZB(
            name='I am an NZB',
            url='http://url-to.nzb/',
        )

        self.assertEqual(nzb.name, 'I am an NZB')
        self.assertEqual(nzb.url, 'http://url-to.nzb/')
        self.assertEqual(str(nzb), '<NZB None>')

    def test_save(self):
        nzb = NZB(
            name='I am an NZB',
            url='http://url-to.nzb/',
        )

        nzb.save()

        nzbs = NZB.query.all()

        self.assertEqual(len(nzbs), 1)
        self.assertEqual(nzb, nzbs[0])
