# Panels

A tool for tracking your comics collection.

## Installation

    git clone
    npm install
    bower install
    gulp
    gulp watch

## Features / WIP

* Browse library
* Search for and add new volumes (from ComicVine)
* Auto search for nzb downloads
* Send downloads to SabNZBd
* Store reading status
* Download comics on to mobile device
* Convert to cbz/cbr (as preferred)
