from bs4 import BeautifulSoup
import requests


class NZBIndexRSS(object):
    url = 'http://nzbindex.nl/rss/'

    @classmethod
    def search(cls, search_terms, search_filter):
        """Search NZBIndex RSS feed for specific terms.  Filter

        @param search_terms: A list of what to search for
        @param search_filter: A RegEx to check each result
        @return: a list of matching nzbs and their name/title.  If the
        server failed to respond then None will be returned
        """
        # Successful matches
        matches = []

        for search_term in search_terms:
            search_response = cls._make_request(search_term)
            if search_response is None:
                return None

            search_results = cls._extract_search_results(search_response)

            matches += cls._filter_search_results(
                search_results,
                search_filter
            )

        return matches

    @classmethod
    def _make_request(cls, search_term, retries=10):
        """Make a request to the search provider

        Poll the server a number of times as NZBIndex is prone to 503s (crap)

        """
        response_text = None
        for attempt in range(0, retries):
            req = requests.get(
                cls.url,
                params={
                    'q': search_term,
                    'age': 1500,
                    'sort': 'agedesc',
                    'max': '500',
                    # 'minsize': '2',
                    'maxsize': '100',
                },
            )

            if req.status_code == requests.codes.ok:
                response_text = req.text
                break

        return response_text

    @classmethod
    def _extract_search_results(cls, search_result):
        soup = BeautifulSoup(search_result)
        items = soup.find_all('item')
        search_results = []
        for item in items:
            title = item.find('title').text
            url = item.find('enclosure').attrs.get('url')
            search_results.append({
                'title': title,
                'url': url,
            })
        return search_results

    @classmethod
    def _filter_search_results(cls, search_results, search_filter):
        matches = []
        for search_result in search_results:
            if search_filter.search(search_result['title']):
                matches.append(search_result)
        return matches
