from test.panels import PanelsTestCase


class PublisherApiTest(PanelsTestCase):
    def test_endpoint_exists(self):
        response = self.client.get('/api/v1/publisher')
        data = response.json

        self.assert200(response)
        self.assertEqual(len(data.get('objects')), 0)
