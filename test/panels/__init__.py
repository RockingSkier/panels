import os

from flask.ext.testing import TestCase

from panels import app, db


_basedir = os.path.abspath(os.path.dirname(__file__))


class PanelsTestCase(TestCase):
    def create_app(self):
        return app

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{db_path}'.format(
            db_path=os.path.join(
                _basedir,
                'panels_test.db',
            )
        )
        self.db = db
        self.db.create_all()

    def tearDown(self):
        self.db.session.remove()
        self.db.drop_all()

    def load_fixture(self, file_path):
        return open(
            os.path.join(
                _basedir,
                'fixtures',
                file_path,
            )
        ).read()
