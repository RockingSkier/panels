var gulp = require('gulp');
var join = require('path').join;
var gulpif = require('gulp-if');
var gutil = require('gulp-util');
var File = gutil.File;
var exclude = require('gulp-ignore').exclude;
var plumber = require('gulp-plumber');
var clean = require('gulp-clean');
var ngmin = require('gulp-ngmin');
var gulpBowerFiles = require('gulp-bower-files');
var less = require('gulp-less');
var templateCache = require('gulp-angular-templatecache');
var htmlmin = require('gulp-htmlmin');
var debug = require('gulp-debug');
var changed = require('gulp-changed');
var notify = require("gulp-notify");

var args = gutil.env;

var GLOB = {
    ALL: '**/*.*',
    JS: '**/*.js',
    HTML: '**/*.html',
    LESS: '**/*.less',
    CSS: '**/*.css'
};
var SRC = {
    SCRIPTS: 'ui/static/app/js/' + GLOB.JS,
    PARTIALS: 'ui/static/app/partials/' + GLOB.HTML,
    LESS: 'ui/static/app/less/' + GLOB.LESS,
    TEMPLATES: 'ui/templates/' + GLOB.HTML
};
var DEST = {
    SCRIPTS: 'panels/ui/static/app/js/',
    PARTIALS: 'panels/ui/static/app/js/',
    CSS: 'panels/ui/static/app/css/',
    TEMPLATES: 'panels/ui/templates/',
    LIBS: 'panels/ui/static/lib/'
};

var plumberOptions = {
    errorHandler: notify.onError("Error: <%= error.message %>")
};

gulp.task('scripts-clean', function () {
    gulp.src(DEST.SCRIPTS + GLOB.JS, {read: false})
        .pipe(plumber(plumberOptions))
        .pipe(exclude(DEST.PARTIALS + 'partials.js'))
        .pipe(debug({verbose: true}))
        .pipe(clean({force: true}));
});

gulp.task('scripts-build', function () {
    gulp.src(SRC.SCRIPTS)
        .pipe(plumber(plumberOptions))
        .pipe(changed(DEST.SCRIPTS))
        .pipe(ngmin())
        .pipe(gulp.dest(DEST.SCRIPTS));
});

// Don't clean automatically as we can compare and update changed files
// If files are added or deleted then clean needs to be run too
gulp.task('scripts', ['scripts-build']);

gulp.task('scripts-watch', function () {
    gulp.watch(SRC.SCRIPTS, ['scripts']);
});


gulp.task('partials-clean', function () {
    gulp.src(DEST.PARTIALS + 'partials.js', {read: false})
        .pipe(plumber(plumberOptions))
        .pipe(clean({force: true}));
});

gulp.task('partials-build', function () {
    gulp.src(SRC.PARTIALS)
        .pipe(plumber(plumberOptions))
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(templateCache('partials.js', {'module': 'pnlsApp'}))
        .pipe(gulp.dest(DEST.PARTIALS));
});

gulp.task('partials', ['partials-clean', 'partials-build']);

gulp.task('partials-watch', function () {
    gulp.watch(SRC.PARTIALS, ['partials']);
});


gulp.task('bower-clean', function () {
    gulp.src(DEST.LIBS + GLOB.ALL, {read: false})
        .pipe(plumber(plumberOptions))
        .pipe(clean({force: true}));
});

gulp.task('bower-build', function () {
    gulpBowerFiles()
        .pipe(plumber(plumberOptions))
        .pipe(gulp.dest(DEST.LIBS));
});

gulp.task('bower', ['bower-clean', 'bower-build']);


gulp.task('templates-clean', function () {
    gulp.src(DEST.TEMPLATES + GLOB.HTML, {read: false})
        .pipe(plumber(plumberOptions))
        .pipe(clean({force: true}));
});

gulp.task('templates-build', function () {
    gulp.src(SRC.TEMPLATES)
        .pipe(plumber(plumberOptions))
        .pipe(changed(DEST.TEMPLATES))
        .pipe(gulp.dest(DEST.TEMPLATES));
});

// Don't clean automatically as we can compare and update changed files
// If files are added or deleted then clean needs to be run too
gulp.task('templates', ['templates-build']);

gulp.task('templates-watch', function () {
    gulp.watch(SRC.TEMPLATES, ['templates']);
});


gulp.task('css-clean', function () {
    gulp.src(DEST.CSS + GLOB.CSS, {read: false})
        .pipe(plumber(plumberOptions))
        .pipe(clean({force: true}));
});


gulp.task('less-build', function () {
    gulp.src(SRC.LESS)
        .pipe(plumber(plumberOptions))
        .pipe(less())
        .pipe(gulp.dest(DEST.CSS));
});

gulp.task('less', ['less-build']);

gulp.task('less-watch', function () {
    gulp.watch(SRC.LESS, ['less']);
});


gulp.task('generate', function () {
    // -c --controller
    // -d --directive

    var controller = args.c || args.controller || null;
    var directive = args.d || args.directive || null;

});
gulp.task('gen', ['generate']); // Alias for generate


gulp.task('watch', [
    'scripts-watch',
    'less-watch',
    'partials-watch',
    'templates-watch'
]);

gulp.task('clean', [
    'bower-clean',
    'scripts-clean',
    'css-clean',
    'partials-clean',
    'templates-clean'
]);


gulp.task('default', [
    'bower',
    'scripts',
    'less',
    'partials',
    'templates'
]);
