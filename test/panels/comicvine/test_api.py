import json
import mock

from test.panels import PanelsTestCase


FIXTURES = {
    'VOLUME_SEARCH': 'volume-search.json',
    'VOLUME_SEARCH_PROCESSED': 'volume-search-processed.json',
}


class ComicVineApiTest(PanelsTestCase):
    @mock.patch('panels.comicvine.model.ComicVine.search_volumes')
    def test_search_volumes_endpoint_exists(self, mock_search_volumes):
        volume_search_data = self.load_fixture(
            FIXTURES['VOLUME_SEARCH_PROCESSED'])
        volume_search_data = json.loads(volume_search_data)
        mock_search_volumes.return_value = volume_search_data

        response = self.client.get(
            '/api/v1/comic-vine/search/volumes?query=Volume%20Name')
        data = response.json

        mock_search_volumes.assert_called_with('Volume Name')

        self.assert200(response)
        self.assertEqual(len(data), 3)

        volume_1001 = data[0]

        # Converted id to pk
        self.assertIn('pk', volume_1001)
        self.assertNotIn('id', volume_1001)

        # Check attributes
        self.assertEqual(volume_1001['pk'], 1001)
        self.assertEqual(volume_1001['name'], 'Volume 1001')
        self.assertEqual(volume_1001['api_url'],
                         'http://www.comicvine.com/api/volume/4050-1001')

        self.assertIn('start_year', volume_1001)
        self.assertIn('publisher', volume_1001)
        self.assertIn('pk', volume_1001['publisher'])
        self.assertIn('name', volume_1001['publisher'])

        # Removed resource_type
        self.assertNotIn('resource_type', volume_1001)
