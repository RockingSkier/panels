import json
import re

import httpretty

from panels.search.providers.nzbindexrss import NZBIndexRSS
from test.panels import PanelsTestCase


FIXTURES = {
    'NZBINDEXRSS_RESPONSE': 'nzbindexrss-response.xml',
    'NZBINDEXRSS_RESPONSE_PROCESSED': 'nzbindexrss-response-processed.json',
}


class NZBIndexRSSTest(PanelsTestCase):
    def setUp(self):
        super(NZBIndexRSSTest, self).setUp()

        self.nzb_index_rss = NZBIndexRSS()

    @httpretty.activate
    def test_search(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://nzbindex.nl/rss/',
            body=self.load_fixture(FIXTURES['NZBINDEXRSS_RESPONSE'])
        )
        search_terms = ['Issue 100001']  # Doesn't really matter as it's a mock
        search_filter = re.compile(
            'issue 100001',
            re.IGNORECASE
        )  # This does matter

        results = self.nzb_index_rss.search(search_terms, search_filter)

        self.assertEqual(len(results), 1)

        expected_result = {
            'title': 'Issue 100001',
            'url': 'http://nzb-url.nzb/100001',
        }
        self.assertDictEqual(results[0], expected_result)

    @httpretty.activate
    def test_search_no_results(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://nzbindex.nl/rss/',
            status=500
        )
        search_terms = ['Issue 100001']  # Doesn't really matter as it's a mock
        search_filter = re.compile(
            'issue 100001',
            re.IGNORECASE
        )  # This does matter

        results = self.nzb_index_rss.search(search_terms, search_filter)

        self.assertIsNone(results)

    @httpretty.activate
    # @mock.patch('requests.get')
    def test__make_request_success(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://nzbindex.nl/rss/',
            responses=[
                httpretty.Response(body='Boo', status=500),
                httpretty.Response(body='Boo', status=500),
                httpretty.Response(body='Boo', status=500),
                httpretty.Response(body='Yay', status=200),
            ]
        )

        response = self.nzb_index_rss._make_request('issue 100001')

        self.assertEqual(response, 'Yay')

    @httpretty.activate
    # @mock.patch('requests.get')
    def test__make_request_500(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://nzbindex.nl/rss/',
            responses=[
                httpretty.Response(body='Boo', status=500),
                httpretty.Response(body='Boo', status=500),
                httpretty.Response(body='Boo', status=500),
                httpretty.Response(body='Yay', status=200),
            ]
        )

        response = self.nzb_index_rss._make_request('issue 100001', 3)

        self.assertIsNone(response)

    def test__extract_search_result(self):
        response_xml = self.load_fixture(FIXTURES['NZBINDEXRSS_RESPONSE'])
        results = self.nzb_index_rss._extract_search_results(response_xml)

        self.assertEqual(len(results), 3)

        expected_results = self.load_fixture(
            FIXTURES['NZBINDEXRSS_RESPONSE_PROCESSED']
        )
        expected_results = json.loads(expected_results)
        self.assertListEqual(results, expected_results)

        # Check each one
        for index, result in enumerate(results):
            self.assertDictEqual(result, expected_results[index])

    def test__filter_search_results(self):
        response_results = self.load_fixture(
            FIXTURES['NZBINDEXRSS_RESPONSE_PROCESSED']
        )
        response_results = json.loads(response_results)

        search_filter = re.compile(
            'issue 100001',
            re.IGNORECASE
        )  # This does matter
        matches = self.nzb_index_rss._filter_search_results(
            response_results,
            search_filter,
        )

        self.assertEqual(len(matches), 1)

        expected_matches = {
            'title': 'Issue 100001',
            'url': 'http://nzb-url.nzb/100001',
        }
        self.assertDictEqual(matches[0], expected_matches)
