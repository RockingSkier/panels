# Serve UI
from flask import render_template
from panels import app


@app.route('/')
def catch_all():
    return render_template('index.html')
