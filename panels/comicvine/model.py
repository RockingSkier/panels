import requests

from panels import app

COMIC_VINE_URLS = {
    'SEARCH': 'http://www.comicvine.com/api/search',
    'ISSUE': 'http://www.comicvine.com/api/issue/4000-{id}',
    'VOLUME': 'http://www.comicvine.com/api/volume/4050-{id}',
    'PUBLISHER': 'http://www.comicvine.com/api/publisher/4010-{id}',
}


class ComicVine():
    """A interface for the ComicVine API

    """

    def __init__(self):
        pass

    @classmethod
    def search_volumes(cls, query=''):
        """Search ComicVine for Volumes

        """
        url = COMIC_VINE_URLS['SEARCH']
        params = cls._get_search_volumes_params(query)

        data = cls._make_request(url, params)
        if data is None:
            return None
        volumes = cls._process_data(data)

        return volumes

    @classmethod
    def get_issue(cls, issue_id=None):
        """Get details for a specific issue

        """
        url = COMIC_VINE_URLS['ISSUE'].format(id=issue_id)
        params = cls._get_get_issue_params()

        return cls._get_data(url, params)

    @classmethod
    def get_volume(cls, volume_id=None):
        """Get details for a specific volume

        """
        url = COMIC_VINE_URLS['VOLUME'].format(id=volume_id)
        params = cls._get_get_volume_params()

        return cls._get_data(url, params)

    @classmethod
    def get_publisher(cls, publisher_id=None):
        """Get details for a specific publisher

        """
        url = COMIC_VINE_URLS['PUBLISHER'].format(id=publisher_id)
        params = cls._get_get_publisher_params()

        return cls._get_data(url, params)

    @classmethod
    def _get_data(cls, url, params):
        data = cls._make_request(url, params)
        if data is None:
            return None
        return cls._process_data(data)

    @staticmethod
    def _make_request(url, params):
        req = requests.get(url, params=params)

        if req.status_code != requests.codes.ok:
            return None
        return req.json()

    @staticmethod
    def _get_default_params():
        return {
            'api_key': app.config.get('COMICVINE_APIKEY'),
            'format': 'json',
        }

    @classmethod
    def _get_search_volumes_params(cls, query):
        params = cls._get_default_params()
        params.update({
            'resources': 'volume',
            'field_list': ','.join([
                'id',
                'name',
                'api_detail_url',
                'start_year',
                'publisher',
                'count_of_issues',
            ]),
            'query': query,
        })
        return params

    @classmethod
    def _get_get_issue_params(cls):
        params = cls._get_default_params()
        params.update({
            'field_list': ','.join([
                'id',
                'name',
                'api_detail_url',
                'issue_number',
                'cover_date',
                'volume',
            ]),
        })
        return params

    @classmethod
    def _get_get_volume_params(cls):
        params = cls._get_default_params()
        params.update({
            'field_list': ','.join([
                'id',
                'name',
                'api_detail_url',
                'publisher',
                'issues',
            ]),
        })
        return params

    @classmethod
    def _get_get_publisher_params(cls):
        params = cls._get_default_params()
        params.update({
            'field_list': ','.join([
                'id',
                'name',
                'api_detail_url',
            ]),
        })
        return params

    @staticmethod
    def _process_data(data):
        return data.get('results', [])
