from panels.search.providers.nzbindexrss import NZBIndexRSS


class Search(object):
    def __init__(self):
        pass

    @staticmethod
    def search(search_term, search_filter):
        results = []

        search_providers = [
            NZBIndexRSS
        ]

        # Search each provider
        for search_provider in search_providers:
            results += search_provider.search(search_term, search_filter)

        return results
