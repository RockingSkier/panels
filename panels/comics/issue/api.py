from panels import manager
from panels.comics.issue.model import Issue

manager.create_api(
    Issue,
    methods=['GET', 'POST', 'DELETE', ],
    collection_name=Issue.__name__.lower(),
    url_prefix='/api/v1',

    # Disable pagination
    max_results_per_page=-1,
    results_per_page=-1,
)
