from panels import manager
from panels.comics.volume.model import Volume

manager.create_api(
    Volume,
    methods=['GET', 'POST', 'DELETE', ],
    collection_name=Volume.__name__.lower(),
    url_prefix='/api/v1',

    # Disable pagination
    max_results_per_page=-1,
    results_per_page=-1,
)
