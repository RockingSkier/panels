from panels import db, Issue
from panels.comicvine.model import ComicVine


class Volume(db.Model):
    """A instance of a volume

    """
    __tablename__ = 'comics_volume'
    pk = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=False,
        unique=True
    )
    name = db.Column(db.String, )
    api_url = db.Column(
        db.String,
        unique=True,
    )

    def __init__(self, pk=None):
        """

        @param pk: int - The Comic Vine id
        """
        self.pk = pk
        self.name = ''
        self.api_url = ''
        self.start_year = None

    def __repr__(self):
        return '<Volume {pk}>'.format(pk=self.pk)

    @classmethod
    def build_from_data(cls, **kwargs):
        """Build a new Volume object using data from the Comic Vine api

        @param kwargs: any data to be applied to the volume
        @return: Volume - a newly created Volume object
        """
        status = None
        volume_id = kwargs.get('id')

        # Attempt to grab issue from DB
        volume = cls.query.get(volume_id)
        if volume is None:
            volume = cls(volume_id)
            status = True

        volume.apply_data(**kwargs)

        # NOTE: No error catching...
        db.session.add(volume)
        db.session.commit()

        return volume, status

    def apply_data(self, **kwargs):
        """Selectively apply passed data to the issue

        @param kwargs: Data to be applied ot the issue
        """
        self.name = kwargs.get('name')
        self.api_url = kwargs.get('api_detail_url')
        self.publisher_pk = kwargs.get('publisher').get('id')
        self.start_year = kwargs.get('start_year')

    def save(self):
        """Save the model to the Database

        """
        db.session.add(self)
        db.session.commit()

    def update(self):
        """Get the latest data from Comic Vine.
        Apply the data, then save to DB

        """
        volume_data = ComicVine.get_volume(self.pk)
        self.apply_data(**volume_data)
        self.save()

    def build_issues(self):
        volume_data = ComicVine.get_volume(self.pk)
        issues_data = volume_data['issues']

        for issue_data in issues_data:
            issue = Issue.query.get(issue_data['id'])
            if issue is None:
                issue = Issue(issue_data['id'])
                issue.volume_pk = self.pk
            issue.update()
