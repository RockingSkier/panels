from panels import manager
from panels.comics.publisher.model import Publisher

manager.create_api(
    Publisher,
    methods=['GET', 'POST', 'DELETE', ],
    collection_name=Publisher.__name__.lower(),
    url_prefix='/api/v1',

    # Disable pagination
    max_results_per_page=-1,
    results_per_page=-1,
)
