import json

import httpretty

from test.panels import PanelsTestCase
from panels.comics.publisher.model import Publisher


FIXTURES = {
    'PUBLISHER_1001': 'publisher-1001.json',
    'PUBLISHER_1001_PROCESSED': 'publisher-1001-processed.json',
}


class PublisherModelTest(PanelsTestCase):
    def test_create(self):
        publisher = Publisher()
        self.assertEqual(str(publisher), '<Publisher None>')

    def test_build_from_data(self):
        publisher_1001_data = self.load_fixture(
            FIXTURES['PUBLISHER_1001_PROCESSED'])
        publisher_1001_data = json.loads(publisher_1001_data)
        publisher, created = Publisher.build_from_data(**publisher_1001_data)

        self.assertTrue(isinstance(publisher, Publisher))
        self.assertEqual(publisher.pk, 1001)
        self.assertEqual(publisher.name, 'Publisher 1001')
        self.assertEqual(publisher.api_url,
                         'http://www.comicvine.com/api/publisher/4010-1001')

        self.assertTrue(created)

        db_publisher = Publisher.query.get(1001)
        self.assertIsNotNone(db_publisher)
        self.assertEqual(publisher.name, 'Publisher 1001')

    def test_build_from_data_existing(self):
        publisher_1001_data = self.load_fixture(
            FIXTURES['PUBLISHER_1001_PROCESSED'])
        publisher_1001_data = json.loads(publisher_1001_data)

        publisher, created = Publisher.build_from_data(**publisher_1001_data)
        self.assertTrue(created)

        # Call this again, second time it should update not create
        publisher, created = Publisher.build_from_data(**publisher_1001_data)
        self.assertFalse(created)

        db_publishers = Publisher.query.all()
        self.assertEqual(len(db_publishers), 1)

    def test_save(self):
        publisher = Publisher(1001)
        publisher.save()

        db_publisher = Publisher.query.get(1001)

        self.assertIsNotNone(db_publisher)
        self.assertEqual(db_publisher.pk, 1001)

    @httpretty.activate
    def test_update(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/publisher/4010-1001',
            body=self.load_fixture(FIXTURES['PUBLISHER_1001'])
        )
        publisher = Publisher(1001)
        publisher.save()

        publisher.update()

        db_publisher = Publisher.query.get(1001)

        self.assertIsNotNone(db_publisher)
        self.assertEqual(db_publisher.pk, 1001)
        self.assertEqual(db_publisher.name, 'Publisher 1001')
        self.assertEqual(db_publisher.api_url,
                         'http://www.comicvine.com/api/publisher/4010-1001')
