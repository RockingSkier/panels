'use strict';

angular.module('pnlsApp')
    .directive('searchNewVolumeRow', function () {
        return {
            templateUrl: 'search-new-volume-row.html',
            restrict: 'A',
            scope: {
                volume: '='
            },
            link: function postLink(scope) {
                scope.add = function () {
                    console.log(scope.volume);
                };
            }
        };
    }
);
