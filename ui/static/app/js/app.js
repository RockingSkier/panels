'use strict';

angular.module('pnlsApp', [
        'ngRoute',
        'ngSanitize',
        'btford.socket-io',
        'ActiveResource'
    ]).config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'main.html',
                controller: 'MainCtrl'
            })
            .when('/search/new-volumes', {
                templateUrl: 'search-new-volumes-results.html',
                controller: 'SearchNewVolumesResultsCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });

    }
).factory('socket', function (socketFactory) {
        return socketFactory();
    }
);
