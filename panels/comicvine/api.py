import json
from flask import request, Response

from panels import app
from panels.comicvine.model import ComicVine


@app.route('/api/v1/comic-vine/search/volumes')
def search():
    query = request.args.get('query', '')
    results = ComicVine.search_volumes(query)
    processed_results = []

    for result in results:
        processed_results.append({
            'pk': result['id'],
            'api_url': result['api_detail_url'],
            'name': result['name'],
            'start_year': result['start_year'],
            'publisher': {
                'pk': result['publisher']['id'],
                'name': result['publisher']['name'],
            },
            'issue_count': result['count_of_issues'],
        })

    return Response(
        json.dumps(processed_results),
        mimetype='application/json',
    )
