from panels import db


class NZB(db.Model):
    """A instance of a volume

    """
    __tablename__ = 'nzb_nzb'
    pk = db.Column(
        db.Integer,
        primary_key=True,
    )
    name = db.Column(db.String, )
    url = db.Column(db.String, )

    def __init__(self, name, url):
        """

        @param name: The name of the NZB
        @param url: The URL of the NZB (to download)
        """
        self.name = name
        self.url = url

    def __repr__(self):
        return '<NZB {pk}>'.format(pk=self.pk)

    def save(self):
        """Save the model to the Database

        """
        db.session.add(self)
        db.session.commit()
