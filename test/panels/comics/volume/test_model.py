import json

import httpretty
import mock
from panels import Issue

from test.panels import PanelsTestCase
from panels.comics.volume.model import Volume


FIXTURES = {
    'VOLUME_1001': 'volume-1001.json',
    'VOLUME_1001_PROCESSED': 'volume-1001-processed.json',
}


class VolumeModelTest(PanelsTestCase):
    def load_volume_1001_data(self):
        volume_1001_data = self.load_fixture(
            FIXTURES['VOLUME_1001_PROCESSED']
        )
        return json.loads(volume_1001_data)

    def create_volume_1001(self):
        volume_1001_data = self.load_volume_1001_data()
        volume, created = Volume.build_from_data(**volume_1001_data)
        return volume

    def test_create(self):
        volume = Volume()
        self.assertEqual(str(volume), '<Volume None>')

    def test_build_from_data(self):
        volume_1001_data = self.load_fixture(
            FIXTURES['VOLUME_1001_PROCESSED'])
        volume_1001_data = json.loads(volume_1001_data)

        volume, created = Volume.build_from_data(**volume_1001_data)
        self.assertTrue(created)
        self.assertEqual(volume.publisher_pk, 1001)

        db_volumes = Volume.query.all()
        self.assertEqual(len(db_volumes), 1)

        self.assertEqual(volume.start_year, 2011)

    def test_build_from_data_update(self):
        volume_1001_data = self.load_fixture(
            FIXTURES['VOLUME_1001_PROCESSED'])
        volume_1001_data = json.loads(volume_1001_data)

        # Create
        volume, created = Volume.build_from_data(**volume_1001_data)
        self.assertTrue(created)

        # Update
        volume, created = Volume.build_from_data(**volume_1001_data)
        self.assertFalse(created)

    def test_save(self):
        volume = Volume(1001)
        volume.save()

        db_publisher = Volume.query.get(1001)

        self.assertIsNotNone(db_publisher)
        self.assertEqual(db_publisher.pk, 1001)

    @httpretty.activate
    def test_update(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/volume/4050-1001',
            body=self.load_fixture(FIXTURES['VOLUME_1001'])
        )

        volume = self.create_volume_1001()

        volume.update()

        db_volume = Volume.query.get(1001)

        self.assertIsNotNone(db_volume)
        self.assertEqual(db_volume.pk, 1001)
        self.assertEqual(db_volume.name, 'Volume 1001')
        self.assertEqual(db_volume.api_url,
                         'http://www.comicvine.com/api/volume/4050-1001')

    @mock.patch('panels.comicvine.model.ComicVine.get_issue')
    @httpretty.activate
    def test_build_issues(self, mock_get_issue):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/volume/4050-1001',
            body=self.load_fixture(FIXTURES['VOLUME_1001'])
        )

        mock_get_issue.return_value = {
            'cover_date': '2014-01-01',
            'volume': {},
        }

        volume = self.create_volume_1001()

        volume.build_issues()

        issue_count = volume.issues.count()
        self.assertEqual(issue_count, 3)

        issue_count = Issue.query.filter_by(
            volume_pk=1001,
        ).count()
        self.assertEqual(issue_count, 3)
