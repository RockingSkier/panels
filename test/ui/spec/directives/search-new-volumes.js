'use strict';

describe('Directive: searchNewVolumes', function () {

    // load the directive's module
    beforeEach(module('pnlsApp'));

    var element,
        scope;

    beforeEach(inject(function ($rootScope) {
        scope = $rootScope.$new();
    }));

    it('should make hidden element visible', inject(function ($compile) {
        element = angular.element('<search-new-volumes></search-new-volumes>');
        element = $compile(element)(scope);
        expect(element.text()).toBe('this is the searchNewVolumes directive');
    }));
});
