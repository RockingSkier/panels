from panels import db
from panels.comicvine.model import ComicVine


class Publisher(db.Model):
    """A instance of a publisher

    """
    __tablename__ = 'comics_publisher'
    pk = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=False,
        unique=True
    )
    name = db.Column(db.String, )
    api_url = db.Column(
        db.String,
        unique=True,
    )

    def __init__(self, pk=None):
        """

        @param pk: int - The Comic Vine id
        """
        self.pk = pk
        self.name = ''
        self.api_url = ''

    def __repr__(self):
        return '<Publisher {pk}>'.format(pk=self.pk)

    @classmethod
    def build_from_data(cls, **kwargs):
        """Build a new Publisher object using data from the Comic Vine api

        @param kwargs: any data to be applied to the publisher
        @return: Publisher - a newly created Publisher object
        """
        status = None
        publisher_id = kwargs.get('id')

        # Attempt to grab issue from DB
        publisher = cls.query.get(publisher_id)
        if publisher is None:
            publisher = cls(publisher_id)
            status = True

        publisher.apply_data(**kwargs)

        # NOTE: No error catching...
        db.session.add(publisher)
        db.session.commit()

        return publisher, status

    def apply_data(self, **kwargs):
        """Selectively apply passed data to the issue

        @param kwargs: Data to be applied ot the issue
        """
        self.name = kwargs.get('name')
        self.api_url = kwargs.get('api_detail_url')

    def save(self):
        """Save the model to the Database

        """
        db.session.add(self)
        db.session.commit()

    def update(self):
        """Get the latest data from Comic Vine.
        Apply the data, then save to DB

        """
        publisher_data = ComicVine.get_publisher(self.pk)
        self.apply_data(**publisher_data)
        self.save()


