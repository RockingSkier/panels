from test.panels import PanelsTestCase


class IssueApiTest(PanelsTestCase):
    def test_endpoint_exists(self):
        response = self.client.get('/api/v1/issue')
        data = response.json

        self.assert200(response)
        self.assertEqual(len(data.get('objects')), 0)
