import re
from dateutil import parser

from panels import db
from panels.comicvine.model import ComicVine
from panels.search.search import Search
from panels.nzb.model import NZB


class Issue(db.Model):
    """A instance of a comic

    """
    __tablename__ = 'comics_issue'
    pk = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=False,
        unique=True
    )
    name = db.Column(db.String, )
    api_url = db.Column(
        db.String,
        unique=True,
    )
    issue_number = db.Column(db.Float, )
    cover_date = db.Column(db.DateTime, )
    volume_pk = db.Column(
        db.Integer,
        db.ForeignKey('comics_volume.pk')
    )
    volume = db.relationship(
        'Volume',
        backref=db.backref('issues', lazy='dynamic')
    )

    def __init__(self, pk=None):
        """

        @param pk: int - The Comic Vine id
        """
        self.pk = pk

    def __repr__(self):
        return '<Issue {pk}>'.format(pk=self.pk)

    @classmethod
    def build_from_data(cls, **kwargs):
        """Create or update an Issue

        If no issue with the given pk exists then create one otherwise
        retrieve and updated the existing one

        @param kwargs: any data to be applied to the issue
        @return: Issue - a newly created Issue object
        @return: Boolean - True: created
                           False: already exists in DB
        """
        status = None
        issue_pk = kwargs.get('id')

        # Attempt to grab issue from DB
        issue = cls.query.get(issue_pk)
        if issue is None:
            issue = cls(issue_pk)
            status = True

        issue.apply_data(**kwargs)

        issue.save()

        return issue, status

    def apply_data(self, **kwargs):
        """Selectively apply passed data to the issue

        @param kwargs: Data to be applied ot the issue
        """
        self.name = kwargs.get('name', self.name)
        self.api_url = kwargs.get('api_detail_url', self.api_url)

        # Volume:
        self.volume_pk = kwargs.get('volume').get('id', self.volume_pk)

        # Issue number: float or int
        issue_number = kwargs.get('issue_number', self.issue_number)
        if issue_number:
            if float(issue_number).is_integer():
                issue_number = int(issue_number)
        self.issue_number = issue_number

        # Cover date: date object
        self.cover_date = parser.parse(kwargs.get('cover_date'))

    def save(self):
        """Save the model to the Database

        """
        db.session.add(self)
        db.session.commit()

    def update(self):
        """Get the latest data from Comic Vine.
        Apply the data, then save to DB

        """
        issue_data = ComicVine.get_issue(self.pk)
        self.apply_data(**issue_data)
        self.save()

    def search(self):
        search_term = self._get_search_terms()
        search_filter = self._get_search_filter()

        results = Search.search(search_term, search_filter)

        self._save_results(results)

        return results

    def _get_search_terms(self):
        search_terms = []

        search_term = '{name} {issue_number} {year}'.format(
            name=self.volume.name,
            issue_number=self._get_issue_number(),
            year=self.cover_date.year,
        )
        search_terms.append(search_term)

        return search_terms

    def _get_search_filter(self):
        # Anything that might be used as a separator
        re_separators = '[\\s\.\#:_-]*'

        # Basic name
        name = self.volume.name
        # Keep only alphanumeric and spaces
        name = ''.join(
            ch for ch in name if ch.isalnum() or ch.isspace() or '-'
        )
        # Split the name
        name_parts = name.split(' ')
        # Allow 'and' or '&'
        for index, name_part in enumerate(name_parts):
            name_parts[index] = name_part.replace('and', '(and|&)')

        # Glue name back together
        re_name = re_separators.join(name_parts)
        # Group name
        re_name = '(?P<name>{re})'.format(re=re_name)

        # Sometimes needed with graphic novels
        re_volumes = '(v(ol(ume)?)?{separators}[0-9]{{0,3}})?'.format(
            separators=re_separators
        )

        # Issue number
        re_issue_numbers = self._get_issue_number()
        # Make floats save
        re_issue_number = re.escape(str(re_issue_numbers))
        # Group name
        re_issue_number = '0*(?P<issue_number>{re})'.format(re=re_issue_number)

        # Allow for multiple covers
        re_covers = '(\([0-9]+\s*covers\))?'

        # Year
        re_year = self.cover_date.year
        # Group name
        re_year = '\(?(?P<year>{re})'.format(re=re_year)

        re_issue = '''
            {name}          # Volume name
            {separators}
            {volumes}       # Volumes data
            {separators}
            {issue_number}  # Issue number
            {separators}
            {covers}        # Number of covers
            {separators}
            {year}          # Published year
        '''.format(
            name=re_name,
            separators=re_separators,
            volumes=re_volumes,
            issue_number=re_issue_number,
            covers=re_covers,
            year=re_year,
        )

        search_filter = re.compile(
            re_issue,
            re.IGNORECASE | re.VERBOSE
        )
        return search_filter

    def _get_issue_number(self):
        if self.issue_number % 1 == 0:
            return int(self.issue_number)
        return float(self.issue_number)

    def _save_results(self, results):
        count = 0

        for result in results:
            nzb = NZB(result['title'], result['url'])
            nzb.save()
            count += 1

        return count
