import json
from dateutil import parser

import httpretty
import mock

from panels.comics.issue.model import Issue
from panels.comics.volume.model import Volume
from panels.nzb.model import NZB
from test.panels import PanelsTestCase


FIXTURES = {
    'ISSUE_100001': 'issue-100001.json',
    'ISSUE_100001_PROCESSED': 'issue-100001-processed.json',
    'BATMAN_TITLES': 'batman-titles.json',
}


class IssueModelTest(PanelsTestCase):
    def load_issue_100001_data(self):
        issue_100001_data = self.load_fixture(
            FIXTURES['ISSUE_100001_PROCESSED']
        )
        return json.loads(issue_100001_data)

    def create_issue_100001(self):
        issue_100001_data = self.load_issue_100001_data()
        issue, created = Issue.build_from_data(**issue_100001_data)
        return issue

    def test_create(self):
        issue = Issue()
        self.assertEqual(str(issue), '<Issue None>')

    def test_build_from_data_new(self):
        issue_100001_data = self.load_issue_100001_data()
        issue, created = Issue.build_from_data(**issue_100001_data)

        self.assertTrue(isinstance(issue, Issue))
        self.assertEqual(issue.pk, 100001)
        self.assertEqual(issue.name, 'Issue 100001')
        self.assertEqual(issue.api_url,
                         'http://www.comicvine.com/api/issue/4000-100001')
        self.assertEqual(issue.issue_number, 1)
        self.assertEqual(issue.cover_date, parser.parse('2014-01-01'))

        self.assertEqual(issue.volume_pk, 1001)

        self.assertTrue(created)

        db_issue = Issue.query.get(100001)
        self.assertIsNotNone(db_issue)
        self.assertEqual('Issue 100001', issue.name)

    def test_build_from_data_existing(self):
        issue_100001_data = self.load_issue_100001_data()
        issue, created = Issue.build_from_data(**issue_100001_data)

        self.assertTrue(created)

        # Call this twice, second time it should update not create
        issue, created = Issue.build_from_data(**issue_100001_data)
        self.assertFalse(created)

        db_issues = Issue.query.all()
        self.assertEqual(len(db_issues), 1)

    def test_save(self):
        issue = Issue(100001)
        issue.save()

        db_issue = Issue.query.get(100001)

        self.assertIsNotNone(db_issue)
        self.assertEqual(db_issue.pk, 100001)

    @httpretty.activate
    def test_update(self):
        httpretty.register_uri(
            httpretty.GET,
            'http://www.comicvine.com/api/issue/4000-100001',
            body=self.load_fixture(FIXTURES['ISSUE_100001'])
        )

        issue = Issue(100001)
        issue.save()

        issue.update()

        db_issue = Issue.query.get(100001)

        self.assertIsNotNone(db_issue)
        self.assertEqual(db_issue.pk, 100001)
        self.assertEqual(db_issue.name, 'Issue 100001')
        self.assertEqual(db_issue.api_url,
                         'http://www.comicvine.com/api/issue/4000-100001')
        self.assertEqual(1, db_issue.issue_number)

    def test_search(self):
        # TODO: Remove Volume from Issue tests
        volume = Volume(1001)
        volume.name = 'Volume 1001'

        issue = Issue(1001)
        issue.name = 'Issue 1001'
        issue.issue_number = 1.1
        issue.cover_date = parser.parse('2013-01-01')
        issue.volume = volume

        expected_results = []
        results = issue.search()

        self.assertListEqual(results, expected_results)

    @mock.patch('panels.search.search.Search.search')
    @mock.patch('panels.comics.issue.model.Issue._save_results')
    @mock.patch('panels.comics.issue.model.Issue._get_search_filter')
    @mock.patch('panels.comics.issue.model.Issue._get_search_terms')
    def test_search_mocked(self,
                           mock__get_search_terms,
                           mock__get_search_filter,
                           mock__save_results,
                           mock_search):
        mock__get_search_terms.return_value = []
        mock__get_search_filter.return_value = ''
        mock__save_results.return_value = 2
        mock_search.return_value = [
            {
                'title': 'title',
                'url': 'url',
            }
        ]

        issue = Issue(1001)

        expected_results = [
            {
                'title': 'title',
                'url': 'url',
            }
        ]
        results = issue.search()

        self.assertEqual(mock__get_search_terms.call_count, 1)
        self.assertEqual(mock__get_search_filter.call_count, 1)
        self.assertEqual(mock__save_results.call_count, 1)
        self.assertEqual(mock_search.call_count, 1)
        self.assertListEqual(results, expected_results)

    def test_regex_results_issue_number_float(self):
        volume = Volume(1001)
        volume.name = 'Batman and Robin'

        issue = Issue(1)
        issue.name = 'Issue 1001'
        issue.cover_date = parser.parse('01-01-2013')
        issue.issue_number = 23.4
        issue.volume = volume

        search_filter = issue._get_search_filter()

        batman_titles = json.loads(self.load_fixture(FIXTURES['BATMAN_TITLES']))

        count = 0
        for batman_title in batman_titles:
            if search_filter.search(batman_title):
                count += 1

        self.assertEqual(count, 10)

    def test_regex_results_issue_number_int(self):
        volume = Volume(1001)
        volume.name = 'Batman and Robin'

        issue = Issue(1)
        issue.name = 'Issue 1001'
        issue.cover_date = parser.parse('01-01-2013')
        issue.issue_number = 23
        issue.volume = volume

        search_filter = issue._get_search_filter()

        batman_titles = json.loads(self.load_fixture(FIXTURES['BATMAN_TITLES']))

        count = 0
        for batman_title in batman_titles:
            if search_filter.search(batman_title):
                print(batman_title)
                count += 1

        self.assertEqual(count, 1)

    def test__get_search_terms(self):
        # TODO: Remove Volume from Issue tests
        volume = Volume(1001)
        volume.name = 'Volume 1001'

        issue = Issue(1001)
        issue.name = 'Issue 1001'
        issue.issue_number = 1.1
        issue.cover_date = parser.parse('2013-01-01')
        issue.volume = volume

        expected_search_terms = [
            'Volume 1001 1.1 2013'
        ]
        search_terms = issue._get_search_terms()

        self.assertListEqual(search_terms, expected_search_terms)

    @mock.patch('panels.comics.issue.model.Issue._get_issue_number')
    def test__get_search_filter(self, mock__get_issue_number):
        mock__get_issue_number.return_value = 1.1

        volume = Volume(1001)
        volume.name = 'Volume 1001'

        issue = Issue(1001)
        issue.name = 'Issue 1001'
        issue.cover_date = parser.parse('2013-01-01')
        issue.volume = volume

        search_filter = issue._get_search_filter()

        # Hard to test something is a RegEx
        try:
            search_filter.match('')
            raises = False
        except AttributeError as e:
            raises = True

        self.assertFalse(raises)

    def test__get_issue_number(self):
        issue = Issue(1001)

        issue.issue_number = 1
        self.assertIs(issue._get_issue_number(), 1)
        self.assertIsNot(issue._get_issue_number(), 1.0)

        issue.issue_number = 1.0
        self.assertIs(issue._get_issue_number(), 1)
        self.assertIsNot(issue._get_issue_number(), 1.0)

        issue.issue_number = 1.1
        self.assertIs(issue._get_issue_number(), 1.1)
        self.assertIsNot(issue._get_issue_number(), 1)

    def test__save_results(self):
        issue = self.create_issue_100001()
        nzb_results = [
            {
                'title': 'title_1',
                'url': 'url_1',
            },
            {
                'title': 'title_2',
                'url': 'url_2',

            },
        ]

        count = issue._save_results(nzb_results)
        nzb_count = NZB.query.count()

        self.assertEqual(count, 2)
        self.assertEqual(nzb_count, 2)
