'use strict';

angular.module('pnlsApp')
    .factory('Volume', function (ActiveResource) {

        var Volume = function (data) {
            this.number('pk');
            this.string('name');
            this.string('start_year');
            this.string('issue_count');
        };

        Volume.inherits(ActiveResource.Base);

        Volume.primaryKey = 'pk';

        Volume.api.set('http://localhost:5000/api/v1');
        Volume.api.indexURL = 'http://localhost:5000/api/v1/volume';
        Volume.api.showURL = 'http://localhost:5000/api/v1/volume/:pk';
        Volume.api.deleteURL = 'http://localhost:5000/api/v1/volume/:pk';
        Volume.api.createURL = 'http://localhost:5000/api/v1/volume';

        return Volume;
    }
);
