'use strict';

angular.module('pnlsApp')
    .directive('searchNewVolumes', function ($http) {
        return {
            templateUrl: 'search-new-volumes.html',
            restrict: 'E',
            replace: true,
            scope: {
                search: '=',
                results: '='
            },
            link: function postLink(scope/*, element, attrs*/) {
                scope.submit = function () {
                    $http.get('/api/v1/comic-vine/search/volumes',
                        {
                            params: {
                                query: scope.search.query
                            }
                        }
                    ).
                        success(function (data/*, status, headers, config*/) {
                            scope.results = data;
                        });

                };
            }
        };
    });
