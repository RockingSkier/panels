from flask import Flask
from flask.ext.restless import APIManager
from flask.ext.socketio import SocketIO
from flask.ext.sqlalchemy import SQLAlchemy

import config

app = Flask(
    __name__,
    template_folder='ui/templates',
    static_folder='ui/static',
)
app.config.from_object(config)
socketio = SocketIO(app)

db = SQLAlchemy(app)

# Restless API
manager = APIManager(app, flask_sqlalchemy_db=db)


# Import models so they get added to the DB
from panels.comics.issue.model import Issue
from panels.comics.volume.model import Volume
from panels.comics.publisher.model import Publisher
from panels.nzb.model import NZB

# Import Restless APIs
from panels.comics.issue import api as issue_api
from panels.comics.volume import api as volume_api
from panels.comics.publisher import api as publisher_api

# Import non-rest endpoints
from panels.comicvine import api

# UI
import ui
